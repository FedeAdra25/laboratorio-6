class CreatePersonas < ActiveRecord::Migration[6.0]
  def change
    create_table :personas do |t|
      t.string :email
      t.string :nombre
      t.string :apellido
      t.date :fecha
      t.timestamps
    end
  end
end
