class CreateViviendas < ActiveRecord::Migration[6.0]
  def change
    create_table :viviendas do |t|
      t.string :tipo_vivienda
      t.string :ciudad
      t.integer :numero
      t.integer :piso
      t.string :depto

      t.timestamps
    end
  end
end
