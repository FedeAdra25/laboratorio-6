class AddReference < ActiveRecord::Migration[6.0]
  def change
    add_reference :personas, :vivienda, foreign_key: true
  end
end
