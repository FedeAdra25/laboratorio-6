class AddImage < ActiveRecord::Migration[6.0]
  def change
    add_column :viviendas, :image_url, :string
  end
end
