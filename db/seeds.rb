# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# encoding: UTF-8



Persona.destroy_all
Vivienda.destroy_all


v1=Vivienda.find_or_create_by(tipo_vivienda:"Casa",
    ciudad:"La Plata",
    numero:1234,
    calle:133,
    piso:0,
    depto:"NA",
    #image_url:"https://www.construyehogar.com/wp-content/uploads/2016/02/Fachada-de-casa-un-piso-moderna-peque%C3%B1a-560x352.jpg"
    image_url:"vivienda_1"
)
v2=Vivienda.find_or_create_by(tipo_vivienda:"Casa",
    ciudad:"La Plata",
    numero:12344,
    calle:133,
    piso:0,
    depto:"NA",
    #image_url:"https://st3.idealista.com/news/archivos/styles/imagen_big_lightbox/public/2018-11/casa_prefabricada.jpg?sv=fmQ3Et0_&itok=pA16oefo"
    image_url:"vivienda_2"
)
v3=Vivienda.find_or_create_by(tipo_vivienda:"Casa",
    ciudad:"La Plata",
    numero:12456,
    calle:133,
    piso:0,
    depto:"NA",
    #image_url:"https://i.pinimg.com/originals/1a/96/37/1a963707986be3931a91f33ac2a19677.jpg"
    image_url:"vivienda_3"

)
v4=Vivienda.find_or_create_by(tipo_vivienda:"Casa",
    ciudad:"La Plata",
    numero:153,
    calle:133,
    piso:0,
    depto:"NA",
    #image_url:"https://i.pinimg.com/originals/5d/7e/80/5d7e8095de543443153a701d2926710f.jpg"
    image_url:"vivienda_4"

)
v5=Vivienda.find_or_create_by(tipo_vivienda:"Casa",
    ciudad:"La Plata",
    numero:103,
    calle:133,
    piso:0,
    depto:"NA",
    #image_url:"https://diseño.vip/wp-content/uploads/2018/03/fachada-de-casa-moderna.jpg"
    image_url:"vivienda_5"

)



Persona.find_or_create_by(email:"mail_testeo2@jaja.com",
    nombre:"Juan",
    apellido:"Perez",
    fecha: Date.new(2005,12,5),
    vivienda:v1
)
Persona.find_or_create_by(email:"mail_testeo3@jaja.com",
    nombre:"Eva",
    apellido:"Perez",
    fecha: Date.new(2006,12,5),
    vivienda:v2
)
Persona.find_or_create_by(email:"mail_testeo4@jaja.com",
    nombre:"Fede",
    apellido:"Perez",
    fecha: Date.new(2007,12,5),
    vivienda:v3
)
Persona.find_or_create_by(email:"mail_testeo5@jaja.com",
    nombre:"Pipu",
    apellido:"Perez",
    fecha: Date.new(2008,12,5),
    vivienda:v4
)
Persona.find_or_create_by(email:"mail_testeo6@jaja.com",
    nombre:"Nacho",
    apellido:"Perez",
    fecha: Date.new(2009,12,5),
    vivienda:v5
)
Persona.find_or_create_by(email:"mail_testeo7@jaja.com",
    nombre:"Mateo",
    apellido:"Perez",
    fecha: Date.new(2010,12,5),
    vivienda:v5
)
Persona.find_or_create_by(email:"mail_testeo8@jaja.com",
    nombre:"Fran",
    apellido:"Perez",
    fecha: Date.new(2011,12,5),
    vivienda:v4
)
Persona.find_or_create_by(email:"mail_testeo9@jaja.com",
    nombre:"Pedrito",
    apellido:"Perez",
    fecha: Date.new(2012,12,5),
    vivienda:v3
)
Persona.find_or_create_by(email:"mail_testeo10@jaja.com",
    nombre:"Tuma",
    apellido:"Perez",
    fecha: Date.new(2013,12,5),
    vivienda:v2
)
Persona.find_or_create_by(email:"mail_testeo11@jaja.com",
    nombre:"Josefa",
    apellido:"Perez",
    fecha: Date.new(2014,12,5),
    vivienda:v1
)