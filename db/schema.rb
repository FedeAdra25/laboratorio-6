# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_10_30_225644) do

  create_table "personas", force: :cascade do |t|
    t.string "email"
    t.string "nombre"
    t.string "apellido"
    t.date "fecha"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "vivienda_id"
    t.index ["vivienda_id"], name: "index_personas_on_vivienda_id"
  end

  create_table "viviendas", force: :cascade do |t|
    t.string "tipo_vivienda"
    t.string "ciudad"
    t.integer "numero"
    t.integer "piso"
    t.string "depto"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "calle"
    t.string "image_url"
  end

  add_foreign_key "personas", "viviendas"
end
