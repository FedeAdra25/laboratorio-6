class Persona < ApplicationRecord
    validates :email, presence:true,uniqueness:true
    validates :nombre, presence:true
    validates :apellido, presence:true
    validates :fecha, presence:true
    belongs_to :vivienda
end
