class Vivienda < ApplicationRecord
    validates :tipo_vivienda, presence:true
    validates :ciudad, presence:true
    validates :calle, presence:true
    validates :numero, presence:true
end
