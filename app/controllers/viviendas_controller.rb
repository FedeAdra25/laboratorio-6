class ViviendasController < ApplicationController
  def index
    @viviendas=Vivienda.all
  end

  def show
    @vivienda=Vivienda.find(params[:id])
  end

  def edit
    @vivienda=Vivienda.find(params[:id])
  end

  def update
    @vivienda=Vivienda.find(params[:id])
    if @vivienda.update(
      params
      .require(:vivienda)
      .permit(:tipo_vivienda, :ciudad, :calle, :numero, :piso, :depto)
    )
      redirect_to viviendas_path
      flash[:success]="Vivienda actualizada"
    else
      render :edit
    end
                
  end


  def new
    @vivienda=Vivienda.new
  end

  def create
    @vivienda=Vivienda
    .new(params
        .require(:vivienda)
        .permit(:tipo_vivienda, :ciudad, :calle, :numero, :piso, :depto))
    if @vivienda.save
      redirect_to viviendas_path
    else
      render :new

    end
  end
end
