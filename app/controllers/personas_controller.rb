class PersonasController < ApplicationController
  def index
    @personas=Persona.all
  end

  def show
  end

  def edit
    @persona=Persona.find(params[:id])
  end

  def update
    @persona=Persona.find(params[:id])
    if @persona.update(
      params
      .require(:persona)
      .permit(:nombre, :apellido, :email, :fecha, :vivienda_id)
    )
      redirect_to personas_path
      flash[:success]="Persona actualizada"
    else
      render :edit
    end
                
  end


  def new
    @persona=Persona.new
  end

  def create
    @persona=Persona.new(params
                              .require(:persona)
                              .permit(:nombre, :apellido,
                                :email, :fecha, :vivienda_id)
                            )
    if @persona.save
      redirect_to personas_path
    else
      render :new

    end
  end
end
