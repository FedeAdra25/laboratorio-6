require 'test_helper'

class ViviendasControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get viviendas_index_url
    assert_response :success
  end

  test "should get show" do
    get viviendas_show_url
    assert_response :success
  end

  test "should get edit" do
    get viviendas_edit_url
    assert_response :success
  end

  test "should get new" do
    get viviendas_new_url
    assert_response :success
  end

end
